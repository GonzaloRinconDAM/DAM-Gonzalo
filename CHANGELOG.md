# Changelog

Cualquier novedad o parche importante se incluirán aquí

Formato basado en [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
y [Semantic Versioning](https://semver.org/spec/v2.0.0.html)

La fecha es en formato `DD-MM-YYYY`

## [0.2.0] 7-12-2022

### Modificaciones

- [Currículum Vitae](./README.md) - Añadido un link a mi GitHub, también una descripción de Neofetch

## [0.1.0] 6-12-2022

### Nuevo

- Creada una carpeta llamada [CV](./CV/)
  - Creado un fichero dentro - [Currículum Vitae](./CV/cv_gonzalo_rincon_es.md)
  - Creada una librería de imágenes - [img](./CV/img/)

### Modificaciones

- [README.md](./README.md) ahora tiene acceso rápido

## [0.0.0] 5-12-2022

### Nuevo

- Creado los archivos `.gitignore` y `CHANGELOG.md`
