# Currículum Vitae - Gonzalo Rincón

>Never stop grindin'

 <img src="./img/ANGYCAT.png" alt="Soy yo" height="200">

Estudio un *Grado Superior en Desarrollo de Aplicaciones Multiplataforma*.
Ah, también soy mu' apañao'.

***

## Contacto

- [gringom936@g.educaand.es](mailto:gringom936@g.educaand.es)
- [Twitter](https://twitter.com/gonZa_rin)
- [GitHub](https://github.com/GonzaloRG-Works)

***

## Experiencia profesional

- Ninguna experiencia laboral **(de momento!)**

## Títulos académicos

1. **Bachillerato** en *Ciencias de la Salud* en
[IES Ribera del Tajo](https://riberadeltajo.es/nuevaweb/)

***

## Habilidades

| Habilidad | Desc | Rating |
|-----------|------|-------:|
| Java        | Lenguaje orientado a objetos | :star: :star: :star: |
| PHP         | Lenguaje aplicado principalmente en páginas web| :star: |
| Python      | Lenguaje de alto nivel aplicable a una amplia variedad de áreas | :star: |
| Videojuegos | Soy una leyenda del gaming | :star: :star: :star: :star: :star: |

## Tarjeta de visita

Aquí tienes mi tarjeta de contacto, en formato JSON

```json
{
    "name" : "Gonzalo",
    "surname" : "Rincón",
    "country" : "Spain",
    "email" : "gringom936@g.educaand.es",
    "phone" : {
            "prefix" : "+34",
            "number" : 627836444
        }
}
```

***

## Extras

Uso Linux, específicamente uso *ArchLinux* (**cómo no...**). Por tanto como buen *Arch user* aquí tienes mi `neofecth` :)
> Neofetch es como una forma de ver las "estadísticas" de un usuario, se usa con ironía para burlarse de quienes lo usaban en un principio lo hacían para presumir.

- Instalación en Arch

```bash
sudo pacman -S neofetch
 ```

- [Instalación en otras plataformas](https://github.com/dylanaraps/neofetch/wiki/Installation)

```bash
                   -`                    zarin@archlinux 
                  .o+`                   --------------- 
                 `ooo/                   OS: Arch Linux x86_64 
                `+oooo:                  Host: VivoBook_ASUSLaptop X509DA_M509DA 1.0 
               `+oooooo:                 Kernel: 6.0.11-arch1-1 
               -+oooooo+:                Uptime: 9 hours, 30 mins 
             `/:-:++oooo+:               Packages: 906 (pacman) 
            `/++++/+++++++:              Shell: bash 5.1.16 
           `/++++++++++++++:             Resolution: 1366x768 
          `/+++ooooooooooooo/`           DE: Plasma 5.26.4 
         ./ooosssso++osssssso+`          WM: KWin 
        .oossssso-````/ossssss+`         WM Theme: Breeze 
       -osssssso.      :ssssssso.        Theme: [Plasma], Breeze [GTK2/3] 
      :osssssss/        osssso+++.       Icons: Adwaita [Plasma], Adwaita [GTK2/3] 
     /ossssssss/        +ssssooo/-       Terminal: konsole 
   `/ossssso+/:-        -:/+osssso+-     Terminal Font: Cascadia Code 10 
  `+sso+:-`                 `.-/+oso:    CPU: AMD Ryzen 5 3500U with Radeon Vega Mobile Gfx (8) @ 2.100GHz 
 `++:.                           `-/+/   GPU: AMD ATI Radeon Vega Series / Radeon Vega Mobile Series 
 .`                                 `/   Memory: 3540MiB / 5877MiB 

                                                                 
                                                                 



```
